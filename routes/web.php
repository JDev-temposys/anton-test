<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/contacts', [
    'uses' => 'ContactController@getContacts',
    'as' => 'contacts.all'
]);

Route::get('/contacts/{id}', [
    'uses' => 'ContactController@showContact',
    'as' => 'contact.show'
]);

Route::get('/contact/new', [
    'uses' => 'ContactController@showCreateContactForm',
    'as' => 'contact.new'
]);

Route::post('/contact/new', [
    'uses' => 'ContactController@createContact',
    'as' => 'contact.create'
]);

Route::post('/contact/update/{id}', [
    'uses' => 'ContactController@updateContact',
    'as' => 'contact.update'
]);
