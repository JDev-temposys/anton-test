<?php

namespace App\Entities;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $table = 'contact_data';

    public function getPropertiesAttribute($value) {
        $value = get_object_vars(json_decode($value));
        return $value ;
    }

    public function user() {
        return $this->belongsTo(User::class);
    }
}
