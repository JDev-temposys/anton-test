<?php

namespace App\Http\Controllers;

use App\Entities\Contact;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    public function getContacts() {
        $contacts = \Auth::user()->contacts;
        return view('contacts.show', ['contacts' => $contacts]);
    }

    public function showContact($id) {
        $contact = Contact::find($id);
        return view('contacts.details', ['contact' => $contact]);
    }

    public function showCreateContactForm() {
        return view('contacts.create');
    }

    public function updateContact($id, Request $request) {
        $data = $request->except('_token', 'contactName');
        $dataNew = [];
        foreach ($data as $temp) {
            $dataNew[$temp[0]] = $temp[1];
        }
        $contact = Contact::find($id);
        $contact->properties = json_encode($dataNew);
        $contact->save();
        return redirect()->route('contacts.all');
    }

    public function createContact (Request $request) {
        $data = $request->except('_token', 'contactName');
        $dataNew = [];
        foreach ($data as $temp) {
            $dataNew[$temp[0]] = $temp[1];
        }
        $contact = new Contact();
        $contact->name = $request->get('contactName');
        $contact->user_id = \Auth::id();
        $contact->properties = json_encode($dataNew);
        $contact->save();
        return redirect()->route('contacts.all');
    }
}
