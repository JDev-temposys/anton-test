
/**
 * First, we will load all of this project's Javascript utilities and other
 * dependencies. Then, we will be ready to develop a robust and powerful
 * application frontend using useful Laravel and JavaScript libraries.
 */

require('./bootstrap');

$( document ).ready(function() {
    var counter = 1;
    $('#add_property').on('click',function () {
        var html = '<div class="row">\n' +
            '                                    <div class="col-sm-6">\n' +
            '                                        <div class="form-group">\n' +
            '                                            <input type="text" name="property_'+ counter +'[]" class="form-control"\n' +
            '                                                   placeholder="Property name">\n' +
            '                                        </div>\n' +
            '                                    </div>\n' +
            '                                    <div class="col-sm-6">\n' +
            '                                        <div class="form-group">\n' +
            '                                            <input type="text" name="property_'+ counter +'[]" class="form-control"\n' +
            '                                                   placeholder="Property value">\n' +
            '                                        </div>\n' +
            '                                    </div>\n' +
            '                                </div>';
        $('#properties').append(html);
        counter ++;
        console.log($('#properties'));
    });
});