@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Your contacts</div>

                    <div class="panel-body">
                        <div class="list-group">
                        @foreach($contacts as $contact)
                            <a href="{{ route('contact.show', ['id' => $contact->id]) }}" class="list-group-item active">
                                {{ $contact->name }}
                            </a>
                        @endforeach
                        </div>
                    </div>
                    <div class="panel-footer">
                        <a href="{{ route('contact.new') }}" class="btn btn-success">Add a contact</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
