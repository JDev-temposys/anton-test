@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Create new contact</div>

                    <div class="panel-body">
                        <form id="contact-form" action="{{ route('contact.create') }}" method="POST">
                            {{ csrf_field() }}

                            <div class="input-group">
                                <span id="contactNameLabel" class="input-group-addon">Contact name</span>
                                <input aria-describedby="contactNameLabel" type="text" class="form-control"
                                       name="contactName" placeholder="Property name">
                            </div>

                            <br>
                            <div id="properties">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <input type="text" name="property_0[]" class="form-control"
                                                   placeholder="Property name">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <input type="text" name="property_0[]" class="form-control"
                                                   placeholder="Property value">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <button type="submit" class="btn btn-success">Save</button>
                        </form>

                    </div>
                    <div class="panel-footer">
                        <a id="add_property" class="btn btn-success">Add a property</a>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection

