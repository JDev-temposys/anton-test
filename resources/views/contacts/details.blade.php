@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">{{ $contact->name }}</div>

                    <div class="panel-body">
                        <form id="contact-form" action="{{route('contact.update', ['id'=> $contact->id])}}" method="POST">
                            {{ csrf_field() }}
                            <div id="properties">
                                <?php $counter = 0?>
                                @foreach($contact->properties as $key => $value)
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <input type="text" name="property_{{$counter}}[]" class="form-control"
                                                       placeholder="Property name"
                                                value="{{$key}}">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <input type="text" name="property_{{$counter}}[]" class="form-control"
                                                       placeholder="Property value"
                                                       value="{{$value}}">
                                            </div>
                                        </div>
                                    </div>
                                    <?php $counter ++; ?>
                                @endforeach
                            </div>
                            <button type="submit" class="btn btn-success">Save</button>
                        </form>
                    </div>
                    <div class="panel-footer">
                        <a id="add_property" class="btn btn-success">Add a property</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
